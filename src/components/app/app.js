import React from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import TodoAdd from '../todo-add';
import './app.css';



class App extends React.Component {
    state = {
        todos: [
            { id: 1, label: 'Drink Coffee', important: false, done: false },
            { id: 2, label: 'Drink tea', important: false, done: false },
            { id: 3, label: 'Drink vodka', important: false, done: false },
        ],
        status: 'all',
        search: '',
    }
    onTodoAdd = (label) => {
        this.setState((oldState) =>{
            const newTodo = {
                id: 25,
                label:label,
                important: false,
            }
            return {todos: [...oldState.todos, newTodo]}

        })
    }

    onSearchFilterChange = searchString => this.setState({search: searchString})

    onSearchFilter = (todos, search) => {
        return todos.filter(todo => todo.label.toLowerCase().includes(search.toLowerCase()))
    }

    onStatusFilterChange = (status) => {
        this.setState({status:status})
    }

    onStatusFilter = (todos, status) => {
        if (status === 'active') {
            return todos.filter(todo => !todo.done)
        }else if (status ==='done'){
            return todos.filter(todo => todo.done)
        }else {
            return todos
        }
    }
    onDone = (doneId) => {
        this.setState((oldState) => {
            const idx = oldState.todos.findIndex(todo => todo.id === doneId)
            const prev = oldState.todos.slice(0, idx)
            const current = oldState.todos[idx]
            const next = oldState.todos.slice(idx + 1)

            const newTodos = [
                ...prev,
                {...current, done: !current.done},
                ...next]

            return {
                todos: newTodos
            }
        })
    }

    onDelete = (id) => {
        this.setState((oldState) => {
            const idx = oldState.todos.findIndex(todo => todo.id === id)

            const prev = oldState.todos.slice(0, idx)
            const next = oldState.todos.slice(idx + 1)

            const newTodos = [...prev, ...next]

            return {
                todos: newTodos
            }
        })
    }

    onImportant = (id) => {
        this.setState((oldState) => {
            const idx = oldState.todos.findIndex(todo => todo.id === id)

            const prev = oldState.todos.slice(0, idx)
            const oldTodo = oldState.todos[idx]
            const newTodo = {...oldTodo, important: !oldTodo.important}
            const next = oldState.todos.slice(idx + 1)

            const newTodos = [...prev, newTodo, ...next]

            return {
                todos: newTodos
            }
        })
    }





    render() {
        const filteredByStatus = this.onStatusFilter(this.state.todos, this.state.status);
        const filteredBySearch = this.onSearchFilter(filteredByStatus, this.state.search);
        const doneTodo = this.state.todos.filter((todo) => todo.done)

        return (
            <div className="todo-app">
                <AppHeader toDo={this.state.todos.length} done={doneTodo.length} />

                <div className="top-panel d-flex">
                    <SearchPanel onSearchFilterChange={this.onSearchFilterChange}/>
                    <ItemStatusFilter onStatusFilterChange={this.onStatusFilterChange}/>
                </div>

                <TodoList onDelete={this.onDelete} onImportant={this.onImportant} onDone={this.onDone} todos={filteredBySearch} />
                <TodoAdd onTodoAdd={this.onTodoAdd}/>
            </div>
        );
    }
};

export default App;