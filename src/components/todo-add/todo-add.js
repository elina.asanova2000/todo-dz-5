import React from 'react';

class TodoAdd extends React.Component{
    state = {
        label:''
    }
    onAdd = (event) => {
        if (this.state.label === '') {
            alert('Write something please!')
        }else {
        this.props.onTodoAdd(this.state.label)
        }
        event.preventDefault()
        this.setState({label: ''})
    }


    render () {
        return (
            <form onSubmit={this.onAdd}>
                <input
                       type="text" value={this.state.label}
                       onChange={(event) => this.setState({label:event.target.value})}/>
                <input type="submit"/>
            </form>
        )
    }
}
export default TodoAdd